//TODO: Cookies!
var players = new Array("Justus", "Peter", "Bob");
var playerCount;
var roleCount = 0;
var rolePool;
var agentShots;
var pathoReveals;
var nomineeCount;
var gameStates;
var currentGameState; //index of the next gameState
var firstNight; //GameStates der ersten Nacht oder false->nicht erste Nacht
var deathHandlers; //custom death handler für jeden Spieler
var mafiosiCount; // >0 -> Mafia in der Nacht aufrufen
var patho; //boolean = tode nicht ansagen?
var unrevealed; //array of dead players that have yet to be revealed

var gameStateOrder = [ //Reihenfolge der GameStates
    //--- Do Not Remove ---// 
    "day",
    "beginning_of_night",
    //--- Night ---//
    "drunk",
    "armor",
    "enarmored",
    "angel",
    "mafia",
    "devil",
    "agent",
    "dracula",
    "pope",
    "witch",
    "zombies",
    //--- Do Not Remove ---//    
    "announce_deaths",
    //--- Checks ---//
    "spy",
    "dete",
    "inspi",
    "whore",
    //--- Do Not Remove ---//
    "end_of_first_night"
];

//proprietary role functions
function custom_mafia_onPool(fake) {
    mafiosiCount++;
    gameStates["mafia"] = function() {
        if(displayGameState("Mafia", "Die Mafia wacht auf und darf jemanden t&ouml;ten.", ".party-mafia") > 0) {
        for(var i = 1; i <= playerCount; i++) {
                if(!$("#players>div:nth-child(" + i + ")").hasClass("dead")) {
                    setActionButton(i, "$(\"#players>div:nth-child(" + i + ")\").addClass(\"kill\");nextGameState()", "<span class='glyphicon glyphicon-screenshot'></span>", "btn-danger");    
                }
            }
        }
    };                                                                                       
    firstNight["mafia"] = function() {
        displayGameState("Mafia", "Die Mafia wacht auf und erkennt sich.", ".party-mafia");
    };
}

function custom_mafia_onKilled(index, reveal) {
    if(!reveal) {
        mafiosiCount--;    
    }
    if((!patho || reveal) && mafiosiCount < 1) {
        delete gameStates["mafia"];
    }
}

function custom_patho_reveal() {
    $("#patho-reveals").html(--pathoReveals);
    $("#players > div.active").removeClass("active");
    for(var i = 0; i < unrevealed.length; i++) {
        $("#players>div:nth-child(" + unrevealed[i] + ")").addClass("active").removeClass("unrevealed");
        deathHandlers[unrevealed[i]-1](unrevealed[i], true);
    }
    unrevealed = new Array();
}

//### Rollen ###//
var availableRoles = [
    {
        name: "Agent",
        party: "citizen", //'mafia', 'citizen', or 'vamp'
        job: "agent", //unique lowercase job id, class 'job-theid' will be added; false = no job
        unique: true, //Ob die Rolle nur einmal vergeben werden kann
        settings: "<span class='glyphicon glyphicon-screenshot'></span> <input type='number' min='0' value='1' id='settings-agent-shots' />",
        count: true, //Ob die Rolle zur Rollenzahl beiträgt
        onApply: function(index) { //Wird aufgerufen, wenn ein Spieler (index = wievielter Spieler) diese Rolle bekommt
            agentShots = $("#settings-agent-shots").val();
            $("#players>div:nth-child(" + index + ") .tag-job").after(" <span class='label label-default'><span class='glyphicon glyphicon-screenshot'></span> <span id='agent-shots'>" + agentShots + "</span></span>");        
        },
        onPool: function(fake) { //wird aufgerufen, wenn die Rolle mindestens einmal im Pool ist oder gefaket wird
            gameStates["agent"] = function() {
                if(displayGameState("Agent", "Der Agent wacht auf und darf jemanden t&ouml;ten.", ".job-agent") > 0 && agentShots > 0) {
                    for(var i = 1; i <= playerCount; i++) {
                        if(!$("#players>div:nth-child(" + i + ")").hasClass("dead")) {
                            setActionButton(i, "$(\"#players>div:nth-child(" + i + ")\").addClass(\"kill\");$(\"#agent-shots\").html(--agentShots);nextGameState()", "<span class='glyphicon glyphicon-screenshot'></span>", "btn-danger");
                        }
                    }
                }    
            };
        },
        onKilled: function(index, reveal) { //reveal: true = only revelation of roles etc., no actual death!
            if(!patho || reveal) {
                delete gameStates["agent"];
            }
        }
    }, {
        name: "Detektiv",
        party: "citizen",
        job: "dete",
        unique: true,
        settings: false,
        count: true,
        onApply: function(index) {},
        onPool: function(fake) {
            gameStates["dete"] = function() {
                displayGameState("Detektiv", "Der Detektiv wacht auf und darf jemanden &uuml;berpr&uuml;fen.", ".job-dete");
            };
            firstNight["dete"] = gameStates["dete"];
        },
        onKilled: function(index, reveal) {
            if(!patho || reveal) {
                delete gameStates["dete"];
            }
        }
    }, {
        name: "Normaler Mafioso",
        party: "mafia",
        job: false,
        unique: false,
        settings: false,
        count: true,
        onApply: function(index) {},
        onPool: custom_mafia_onPool,
        onKilled: custom_mafia_onKilled
    }, {
        name: "Pathologe",
        party: "citizen",
        job: "patho",
        unique: true,
        settings: "<span class='glyphicon glyphicon-eye-open'></span> <input type='number' min='0' value='1' id='settings-patho-reveals' />",
        count: true,
        onApply: function(index) {
            pathoReveals = $("#settings-patho-reveals").val();
            $("#players>div:nth-child(" + index + ") .tag-job").after(" <span class='label label-default'><span class='glyphicon glyphicon-eye-open'></span> <span id='patho-reveals'>" + pathoReveals + "</span></span>");
        },
        onPool: function(fake) {
            patho = true;
        },
        onKilled: function(index, reveal) {
            //nothing to do here, can't be revealed ;D    
        }
    }, {
        name: "Politiker",
        party: "mafia",
        job: "poli",
        unique: true,
        settings: false,
        count: true,
        onApply: function(index) {
            $("#players>div:nth-child(" + index + ")").removeClass("detected-mafia").addClass("detected-citizen");
            $("#players>div:nth-child(" + index + ") .tag-detected").html("B&uumlrger");
        },
        onPool: custom_mafia_onPool,
        onKilled: custom_mafia_onKilled
    }
];

//init script
$(function() {
    updatePlayerList();
    updateRoleCount(0);
    generateRoleList();    
});

function addPlayer() {
    var name = $("#add-player-name").val().trim();
    $("#add-player-name").val("");
    if(name == "" || $.inArray(name, players) != -1) return;
    players.push(name);
    Cookies.set("players", players, { expires: 365, path: '' });
    updatePlayerList();
}

function updatePlayerList() {
    $(".player-count").html(players.length);
    if(players.length == 0) {
        $("#player-list").html("<p class='text-muted'>Es wurden noch keine Spieler hinzugef&uuml;gt.</p>");
        return;
    }
    var table = "<table class='table table-striped'>";
    for(var i = 0; i < players.length; i++) {
        table += "<tr><td>" + players[i] + "</td>";
        table += "<td><button class='btn btn-danger btn-xs' onclick='removePlayer(" + i + ")'><span class='glyphicon glyphicon-remove'></span></button></td></tr>"
    }
    table += "</table>";
    $("#player-list").html(table);
}

function removePlayer(index) {
    players.splice(index, 1);
    updatePlayerList();
}

function generateRoleList() {
    var table = "";
    for(var i = 0; i < availableRoles.length; i++) {
        table += "<tr><td>";
        if(availableRoles[i].unique) {
            table += "<input class='role-enabled' type='checkbox' onchange='updateRoleCount(this.checked ? 1 : -1)' />";
        } else {
            table += "<input type='number' min='0' value='0' class='amount' onfocus='this.previousValue = this.value' onchange='updateRoleCount(this.value-this.previousValue); this.previousValue = this.value' />";
        }
        table += "</td><td>" + availableRoles[i].name + "</td>";
        table += "<td>" + (availableRoles[i].settings !== false ? availableRoles[i].settings : "") + "</td>";
        table += "<td><input class='role-fake' type='checkbox' /> Fake</td></tr>";    
    }
    $("#role-list").html(table);
}

function updateRoleCount(change) {
    roleCount += change;
    $(".role-count").html(roleCount);
    if(roleCount < players.length) {
        $(".role-count").addClass("role-count-error");
        $("#start-game-btn").prop("disabled", true);    
    } else {
        $(".role-count").removeClass("role-count-error"); 
        $("#start-game-btn").prop("disabled", false);
    }    
}

function startGame() {
    //generate players
    var playerlist = "";
    playerCount = players.length;
    for(var i = 0; i < playerCount; i++) {
        playerlist += "<div><span class='player-name'><span class='tag-unrevealed'><span class='glyphicon glyphicon-eye-close'></span> </span>" + players[i] + "</span> ";
        playerlist += "<span class='label label-default tag-job'></span> ";
        playerlist += "<span class='label label-default tag-party'></span> ";
        playerlist += "<span class='label label-default tag-detected-parent'><span class='glyphicon glyphicon-search'></span> <span class='tag-detected'></span></span> ";
        playerlist += "<span class='label label-success tag-guarded'><span class='glyphicon glyphicon-ban-circle'></span></span> ";
        playerlist += "<span class='label label-danger tag-kill'><span class='glyphicon glyphicon-screenshot'></span></span> ";
        playerlist += "<span class='label label-primary tag-nominated-parent'><span class='glyphicon glyphicon-flag'></span> <span class='tag-nominated'></span></span> ";
        playerlist += "<span class='action-btn'></span> ";
        playerlist += "<div class='overlay'></div>";
        playerlist += "</div>";
    }
    $("#players").html(playerlist);
    
    //resets
    rolePool = new Array();
    unrevealed = new Array();
    gameStates = new Object(); //assoc array
    firstNight = new Object(); //assoc array
    mafiosiCount = 0;
    patho = false;
    
    //process role settings    
    for(var i = 0; i < availableRoles.length; i++) {
        if(availableRoles[i].unique) {
            if($("#role-list tr:nth-child("+(i+1)+") .role-enabled").prop("checked")) {
                rolePool.push(availableRoles[i]);
                availableRoles[i].onPool(false);
            } else if($("#role-list tr:nth-child("+(i+1)+") .role-fake").prop("checked")) {
                availableRoles[i].onPool(true);
            } 
        } else {
            var amount = $("#role-list tr:nth-child("+(i+1)+") .amount").val();
            if(amount > 0) {
                for(var j = 0; j < amount; j++) {
                    rolePool.push(availableRoles[i]);
                    availableRoles[i].onPool(false);
                }    
            } else if($("#role-list tr:nth-child("+(i+1)+") .role-fake").prop("checked")) {
                availableRoles[i].onPool(true);
            }
            
        }
    }
    
    deathHandlers = new Array();
    
    //apply roles randomly
    for(var i = 1; i <= playerCount; i++) {
        var role = getRandomRole();
        if(role.job !== false) {
            $("#players>div:nth-child(" + i + ") .tag-job").html(role.name);
            $("#players>div:nth-child(" + i + ")").addClass("job-" + role.job);    
        } else {
            $("#players>div:nth-child(" + i + ") .tag-job").html("[arbeitslos]");
        }
        if(role.party == "citizen") {
            $("#players>div:nth-child(" + i + ")").addClass("party-citizen detected-citizen");
            $("#players>div:nth-child(" + i + ") .tag-detected").html("B&uuml;rger");    
            $("#players>div:nth-child(" + i + ") .tag-party").html("B&uuml;rger");    
        } else if(role.party == "mafia") {
            $("#players>div:nth-child(" + i + ")").addClass("party-mafia detected-mafia");
            $("#players>div:nth-child(" + i + ") .tag-detected").html("Mafia");
            $("#players>div:nth-child(" + i + ") .tag-party").html("Mafia");    
        }
        role.onApply(i); //trigger custom apply-handler
        deathHandlers.push(role.onKilled); //store custom kill handler   
    }
    
    //### non-role-gameStates ###// 
    //beginning of nights
    gameStates["beginning_of_night"] = function() {
        $("#players>div.nominated").removeClass("nominated");
        $("#header").addClass("night").removeClass("day");
        displayGameState("Nacht", "Alle schlafen ein.", false);
    };
    firstNight["beginning_of_night"] = gameStates["beginning_of_night"];   
    //transition from first night to normal ones
    firstNight["end_of_first_night"] = function() {
        firstNight = false;
        currentGameState = 0;
        nextGameState();    
    };   
    //day
    gameStates["day"] = function() {    
        //a bit of hardcode for the patho
        if(displayGameState("Tag", "Alle wachen auf.", ".job-patho", false) > 0 && pathoReveals > 0) {
            $("#custom-header-buttons").append('<button class="btn btn-default" onclick="custom_patho_reveal()"><span class="glyphicon glyphicon-eye-open"></span> Aufdecken</button>');    
        }
        $("#header").addClass("day").removeClass("night");
        $("#players>div").removeClass("kill guarded");
        
        //absolutely realistic and reasonable procedure of hanging someone randomly every day
        nomineeCount = 0;
        for(var i = 1; i <= playerCount; i++) {
            if(!$("#players>div:nth-child(" + i + ")").hasClass("dead")) {
                setActionButton(i, "$(\"#players>div:nth-child(" + i + ")\").addClass(\"nominated\");$(\"#players>div:nth-child(" + i + ") .tag-nominated\").html(++nomineeCount);$(\"#players>div:nth-child(" + i + ") .action-btn\").html(\"\")", "<span class='glyphicon glyphicon-flag'></span>", "btn-primary");
            }
        }
        $("#custom-header-buttons").append('<button class="btn btn-primary" onclick="endNominations()"><span class="glyphicon glyphicon-flag"></span> Nominierungen schließen</button>');
    }
    //announce deaths
    gameStates["announce_deaths"] = function() {
        //automatically process deaths
        for(var i = 1; i <= playerCount; i++) {
            var player = $("#players>div:nth-child(" + i + ")");
            if(!player.hasClass("dead") && (player.hasClass("devil-kill") || (player.hasClass("kill") && !player.hasClass("guarded")))) {
                player.addClass("recently-killed");
                kill(i);
            }
        }
        if(displayGameState("Tode", "Gestorben sind" + (patho ? " <b>(KEINE ROLLEN ETC. ANSAGEN!)</b>" : "") + ":", ".recently-killed", false) == 0) {
            $("#instructions").html("Es ist niemand gestorben.");
        }
        $("#players>div.recently-killed").removeClass("recently-killed").addClass("dead");    
    }; 
    
    //start the game
    $("#next-gamestate-btn").removeClass("hide");
    currentGameState = 0;
    nextGameState();   
}

function getRandomRole() { ;
    return rolePool.splice(Math.floor(Math.random() * rolePool.length), 1)[0];
}

function nextGameState() {
    currentGameState++;
}

function displayGameState(title, instructions, playerFilter, fakeable) {
    fakeable = typeof fakeable !== "undefined" ? fakeable : true; //default value

    //reset player highlighting + action buttons
    $("#players > div.active").removeClass("active");
    $("#players > div .action-btn").html("");
    $("#custom-header-buttons").html("");
    
    var amount = 0;
    if(playerFilter !== false) {
        amount = $("#players > div" + playerFilter + ":not(.dead)").addClass("active").length;
        if(amount == 0 && fakeable) {
            title += " (Fake)";            
        }
    }
    $("#header > h1").html(title);
    $("#instructions").html(instructions);
    return amount;    
}

function kill(index) {
    var player = $("#players>div:nth-child(" + index + ")");
    if(patho) {
        unrevealed.push(index);
        player.addClass("unrevealed");
    }
    //trigger custom death handler
    deathHandlers[index-1](index, false);
    player.addClass("recently-killed")    
}

function nextGameState() {
    //determine wether it's the first night
    var gs = firstNight === false ? gameStates : firstNight;
    //skip non-existent gameStates
    while(typeof gs[gameStateOrder[currentGameState]] === "undefined") {
        currentGameState++;
        //reloop on out-of-bounds
        if(currentGameState >= gameStateOrder.length) {
            currentGameState = 0;
        }
    }
    //trigger custom gameState handler
    currentGameState++;
    gs[gameStateOrder[currentGameState-1]]();    
}

function setActionButton(player, onClick, content, cssClass) {
    $("#players>div:nth-child(" + player + ") .action-btn").html("<button class='btn btn-sm " + cssClass + "' onclick='" + onClick + "'>" + content + "</button>");    
}

function endNominations() {
    $("#players > div.active").removeClass("active");
    for(var i = 1; i <= playerCount; i++) {
        $("#players>div:nth-child(" + i + ") .action-btn").empty();
        var player = $("#players>div:nth-child(" + i + ")");
        if(player.hasClass("nominated") && !player.hasClass("alibi")) {
            setActionButton(i, "elect(" + i + ")", "<span class='glyphicon glyphicon-screenshot'></span>", "btn-danger");
            player.addClass("active");
        }
    }    
}

function elect(index) {
    kill(index);
    $("#players>div.recently-killed").removeClass("recently-killed").addClass("dead");
    if(patho) {
        $("#instructions").html("<b>NICHT DIE ROLLE ETC. ANSAGEN!</b>");
    }
    $("#custom-header-buttons > button:last-child()").remove();
    $("#players>div:nth-child(" + index + ") .action-btn").html("");
} 